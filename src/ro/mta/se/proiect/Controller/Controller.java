package ro.mta.se.proiect.Controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import ro.mta.se.proiect.Model.Board;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {
    ContextMenu contextMenu = new ContextMenu();
    Board board = new Board();
    @FXML
    GridPane Grid1;
    @FXML
    GridPane Grid2;
    @FXML
    Button ButtonReady;
    Alert gameOver = new Alert(Alert.AlertType.INFORMATION);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                GridPane g = new GridPane();
                g.setMaxHeight(24);
                g.setMaxWidth(19);
                GridPane.setConstraints(g, i, j);
                GridPane g2 = new GridPane();
                g2.setMaxHeight(24);
                g2.setMaxWidth(19);
                GridPane.setConstraints(g2, i, j);
                g.setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.isSecondaryButtonDown()) {
                            System.out.println(GridPane.getColumnIndex(g) + " " + GridPane.getRowIndex(g));
                            contextMenu.getItems().clear();
                            contextMenu(event.getScreenX(), event.getScreenY(), g, GridPane.getColumnIndex(g), GridPane.getRowIndex(g));
                        }

                    }
                });
                g2.setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.isSecondaryButtonDown()) {
                            if (board.nrHitted != 3 && board.Ready == true) {
                                System.out.println(GridPane.getColumnIndex(g2) + " " + GridPane.getRowIndex(g2));
                                Hit(GridPane.getColumnIndex(g2), GridPane.getRowIndex(g2));
                            }
                        }

                    }
                });
                ButtonReady.setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.isPrimaryButtonDown()) {
                            System.out.println("Ready");
                            board.Ready = true;
                        }
                    }
                });
                Grid1.getChildren().add(g);
                Grid2.getChildren().add(g2);
            }
        }
    }

    public void Hit(int x, int y) {
        if (board.matBoard[y][x] != 0) {
            Grid1.getChildren().get(x * 15 + y + 1).setStyle("-fx-background-color: #B0171F;");
            Grid2.getChildren().get(x * 15 + y + 1).setStyle("-fx-background-color: #B0171F;");
        } else {
            Grid1.getChildren().get(x * 15 + y + 1).setStyle("-fx-background-color: #000000;");
            Grid2.getChildren().get(x * 15 + y + 1).setStyle("-fx-background-color: #000000;");
        }
        if (board.matBoard[y][x] == 4) {
            for (int i = 0; i < 15; i++)
                for (int j = 0; j < 15; j++)
                    if (board.matBoard[i][j] == 1 || board.matBoard[i][j] == 4) {
                        Grid1.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                        Grid2.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                    }
            board.nrHitted++;
        }
        if (board.matBoard[y][x] == 5) {
            for (int i = 0; i < 15; i++)
                for (int j = 0; j < 15; j++)
                    if (board.matBoard[i][j] == 2 || board.matBoard[i][j] == 5) {
                        Grid1.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                        Grid2.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                    }
            board.nrHitted++;
        }
        if (board.matBoard[y][x] == 6) {
            for (int i = 0; i < 15; i++)
                for (int j = 0; j < 15; j++)
                    if (board.matBoard[i][j] == 3 || board.matBoard[i][j] == 6) {
                        Grid1.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                        Grid2.getChildren().get(j * 15 + i + 1).setStyle("-fx-background-color: #B0171F;");
                    }
            board.nrHitted++;
        }
        if (board.nrHitted == 3) {
            gameOver.setTitle("Game Over");
            gameOver.setContentText("Game Over");
            gameOver.showAndWait();
        }


    }

    public void checkReady() {
        if (board.nrPlanes == 3) {
            ButtonReady.setDisable(false);
        } else {
            ButtonReady.setDisable(true);
        }
    }

    public void colorPlanes(int[][] boardMat) {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                if (board.matBoard[j][i] == 1) {
                    Grid1.getChildren().get(i * 15 + j + 1).setStyle("-fx-background-color: #5075c0;");
                }
                if (board.matBoard[j][i] == 4) {
                    Grid1.getChildren().get(i * 15 + j + 1).setStyle("-fx-background-color: #4876FF;");
                }
                System.out.print(board.matBoard[i][j] + " ");
            }
            System.out.println();
        }
        checkReady();
    }

    public EventHandler<javafx.event.ActionEvent> drawPlane1(int x, int y) {
        return new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                MenuItem item = (MenuItem) event.getSource();

                String direction = item.getText();
                board.drowPlane(direction, y, x);
                colorPlanes(board.matBoard);
//                if (plane.getPlanesNo() == 3) {
//                    planeNoAlert();
//                    return;
//                }
//
//                if ("North".equalsIgnoreCase(direction)) {
//                    plane.drawPlane(x, y, direction);
//
//
//                }
            }
        };
    }

    public void contextMenu(double screenX, double screenY, GridPane tile, int x, int y) {
        EventHandler<ActionEvent> NorthHandler = drawPlane1(x, y);
        EventHandler<javafx.event.ActionEvent> EastHandler = drawPlane1(x, y);
        EventHandler<javafx.event.ActionEvent> WestHandler = drawPlane1(x, y);
        EventHandler<javafx.event.ActionEvent> SouthHandler = drawPlane1(x, y);
        EventHandler<javafx.event.ActionEvent> DeleteHandler = deletePlane(x, y);
        MenuItem north = new MenuItem("up");
        north.setOnAction(NorthHandler);
        MenuItem east = new MenuItem("right");
        east.setOnAction(EastHandler);
        MenuItem west = new MenuItem("left");
        west.setOnAction(WestHandler);
        MenuItem south = new MenuItem("down");
        south.setOnAction(SouthHandler);
        MenuItem delete = new MenuItem("Delete");
        delete.setOnAction(DeleteHandler);

        contextMenu.getItems().addAll(north, south, east, west);
        if (!board.checkDirections("up", x, y))
            contextMenu.getItems().remove(north);
        if (!board.checkDirections("down", x, y))
            contextMenu.getItems().remove(south);
        if (!board.checkDirections("right", x, y))
            contextMenu.getItems().remove(east);
        if (!board.checkDirections("left", x, y))
            contextMenu.getItems().remove(west);
        if (board.nrPlanes > 2)
            contextMenu.getItems().clear();
        if (board.checkDelee(x, y))
            contextMenu.getItems().add(delete);
        if (board.Ready == true)
            contextMenu.getItems().clear();
        contextMenu.show(tile, screenX, screenY);
    }

    private EventHandler<ActionEvent> deletePlane(int y, int x) {
        return new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MenuItem item = (MenuItem) event.getSource();
                if (board.matBoard[x][y] == 1 || board.matBoard[x][y] == 4) {
                    for (int i = 0; i < 15; i++)
                        for (int j = 0; j < 15; j++)
                            if (board.matBoard[i][j] == 1 || board.matBoard[i][j] == 4)
                                board.matBoard[i][j] = 0;
                    for (int i = 0; i < 15; i++)
                        for (int j = 0; j < 15; j++)
                            if (board.matBoard[i][j] != 0)
                                board.matBoard[i][j]--;
                }
                if (board.matBoard[x][y] == 2 || board.matBoard[x][y] == 5) {
                    for (int i = 0; i < 15; i++)
                        for (int j = 0; j < 15; j++)
                            if (board.matBoard[i][j] == 2 || board.matBoard[i][j] == 5)
                                board.matBoard[i][j] = 0;
                    for (int i = 0; i < 15; i++)
                        for (int j = 0; j < 15; j++)
                            if (board.matBoard[i][j] != 0 && board.matBoard[i][j] != 1 && board.matBoard[i][j] != 4)
                                board.matBoard[i][j]--;
                }
                if (board.matBoard[x][y] == 3 || board.matBoard[x][y] == 6) {
                    for (int i = 0; i < 15; i++)
                        for (int j = 0; j < 15; j++)
                            if (board.matBoard[i][j] == 3 || board.matBoard[i][j] == 6)
                                board.matBoard[i][j] = 0;
                }

                for (int i = 0; i < 15; i++) {
                    for (int j = 0; j < 15; j++) {
                        if (board.matBoard[j][i] == 0) {
                            Grid1.getChildren().get(i * 15 + j + 1).setStyle("-fx-background-color: #FFFFF;");
                        }
                    }
                }
                board.nrPlanes--;
                checkReady();
            }
        };
    }
}
