package ro.mta.se.proiect.Model;


public class Plane {
    int x;
    int y;
    int [][] matPlane;

    public Plane(int x, int y, String direction) {
        matPlane=new int [15][15];
        this.x = x;
        this.y = y;
        if (direction == "up") {
            matPlane[x][y] = 3;
            matPlane[x - 2][y] = 1;
            for(int j=y-2;j<y+3;j++)
                matPlane[x-1][j]=1;
            for(int j=y-1;j<y+2;j++)
                matPlane[x-3][j]=1;
        }
    }
}
