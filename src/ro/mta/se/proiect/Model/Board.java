package ro.mta.se.proiect.Model;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;


public class Board {
    //public GridPane Grid1;
    public int[][] matBoard;
    public int nrPlanes;
    public boolean Ready;
    public int nrHitted;

    public Board() {
        Ready = false;
        nrHitted=0;
        nrPlanes = 0;
        matBoard = new int[15][15];
        for (int i = 0; i < 15; i++)
            for (int j = 0; j < 15; j++)
                matBoard[i][j] = 0;
    }

    public void drowPlane(String direction, int x, int y) {

        for (int i = 0; i < 15; i++)
            for (int j = 0; j < 15; j++)
                if (matBoard[i][j] != 0)
                    matBoard[i][j]++;

        if (direction == "up") {
            matBoard[x][y] = 4;
            matBoard[x - 2][y] = 1;
            for (int j = y - 2; j < y + 3; j++)
                matBoard[x - 1][j] = 1;
            for (int j = y - 1; j < y + 2; j++)
                matBoard[x - 3][j] = 1;
        }
        if (direction == "down") {
            matBoard[x][y] = 4;
            matBoard[x + 2][y] = 1;
            for (int j = y - 2; j < y + 3; j++)
                matBoard[x + 1][j] = 1;
            for (int j = y - 1; j < y + 2; j++)
                matBoard[x + 3][j] = 1;
        }
        if (direction == "left") {
            matBoard[x][y] = 4;
            matBoard[x][y - 2] = 1;
            for (int j = x - 2; j < x + 3; j++)
                matBoard[j][y - 1] = 1;
            for (int j = x - 1; j < x + 2; j++)
                matBoard[j][y - 3] = 1;
        }
        if (direction == "right") {
            matBoard[x][y] = 4;
            matBoard[x][y + 2] = 1;
            for (int j = x - 2; j < x + 3; j++)
                matBoard[j][y + 1] = 1;
            for (int j = x - 1; j < x + 2; j++)
                matBoard[j][y + 3] = 1;
        }

        nrPlanes++;
    }

    public boolean checkDelee(int y, int x) {
        if (matBoard[x][y] != 0)
            return true;
        return false;
    }

    public boolean checkDirections(String direction, int y, int x) {
        if (direction == "up" && x >= 3 && y <= 12 && y >= 2) {
            if (matBoard[x][y] != 0)
                return false;
            if (matBoard[x - 2][y] != 0)
                return false;
            for (int j = y - 2; j < y + 3; j++)
                if (matBoard[x - 1][j] != 0)
                    return false;
            for (int j = y - 1; j < y + 2; j++)
                if (matBoard[x - 3][j] != 0)
                    return false;
            return true;
        }
        if (direction == "down" && x <= 11 && y <= 12 && y >= 2) {
            if (matBoard[x][y] != 0)
                return false;
            if (matBoard[x + 2][y] != 0)
                return false;
            for (int j = y - 2; j < y + 3; j++)
                if (matBoard[x + 1][j] != 0)
                    return false;
            for (int j = y - 1; j < y + 2; j++)
                if (matBoard[x + 3][j] != 0)
                    return false;
            return true;
        }
        if (direction == "left" && y >= 3 && x >= 2 && x <= 12) {
            if (matBoard[x][y] != 0)
                return false;
            if (matBoard[x][y - 2] != 0)
                return false;
            for (int j = x - 2; j < x + 3; j++)
                if (matBoard[j][y - 1] != 0)
                    return false;
            for (int j = x - 1; j < x + 2; j++)
                if (matBoard[j][y - 3] != 0)
                    return false;
            return true;
        }
        if (direction == "right" && x >= 2 && x <= 12 && y <= 11) {
            if (matBoard[x][y] != 0)
                return false;
            if (matBoard[x][y + 2] != 0)
                return false;
            for (int j = x - 2; j < x + 3; j++)
                if (matBoard[j][y + 1] != 0)
                    return false;
            for (int j = x - 1; j < x + 2; j++)
                if (matBoard[j][y + 3] != 0)
                    return false;
            return true;
        }
        return false;
    }
}
