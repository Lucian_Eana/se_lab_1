package ro.mta.se.proiect;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        try {
            SplitPane board = (SplitPane) FXMLLoader.load(Main.class.getResource("View/plane.fxml"));
            Scene scene = new Scene(board);
            primaryStage.setScene((scene));
            primaryStage.setTitle("Planes");
            primaryStage.setResizable(true);
            primaryStage.show();
        } catch (IOException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
