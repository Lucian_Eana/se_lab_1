package ro.mta.se.lab1;

import tema3.Animal;
import tema3.Carnivore;
import tema3.Herbivore;
import tema3.Omnivorous;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucian Eana on 11/20/2016.ab
 */
public class Main {
    public static void main(String[] arguments) {
        try {
            List<String> list = new ArrayList<String>();
            List<Animal> animalList = new ArrayList<Animal>();
            Read(arguments[0], list);
            for (int i = 0; i < list.size(); i += 5) {
                if (list.get(i + 1).equals("Omnivorous")) {
                    int cotet = Integer.parseInt(list.get(i + 2));
                    double cantitate = Double.valueOf(list.get(i + 3));
                    long interval = Long.parseLong(list.get(i + 4));
                    Animal animall = new Omnivorous(list.get(i), cotet, cantitate, interval);
                    animalList.add(animall);
                }
                if (list.get(i + 1).equals("Herbivore")) {
                    int cotet = Integer.parseInt(list.get(i + 2));
                    double cantitate = Double.valueOf(list.get(i + 3));
                    long interval = Long.parseLong(list.get(i + 4));
                    Animal animall = new Herbivore(list.get(i), cotet, cantitate, interval);
                    animalList.add(animall);
                }
                if (list.get(i + 1).equals("Carnivore")) {
                    int cotet = Integer.parseInt(list.get(i + 2));
                    double cantitate = Double.parseDouble(list.get(i + 3));
                    long interval = Long.parseLong(list.get(i + 4));
                    Animal animall = new Carnivore(list.get(i), cotet, cantitate, interval);
                    animalList.add(animall);
                }
            }
            for (Animal animal : animalList) {
                animal.eat();
            }
        } catch (Exception e) {

        }
    }

    public static void Read(String input, List<String> output) {
        BufferedReader buffer = null;
        String[] line;

        String CurrentLine;
        try {
            buffer = new BufferedReader(new FileReader(input));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while ((CurrentLine = buffer.readLine()) != null) {
                CurrentLine = CurrentLine.replaceAll(",", "");
                CurrentLine = CurrentLine.replaceAll("\\s+", " ");
                line = CurrentLine.split(" ");
                for (String item : line) {
                    output.add(item);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}