package ro.mta.se.lab5.observers;

public class Hours implements TimeObserver {
    private long hours;

    public void update(long time) {
        hours = (int) ((time / (1000 * 60 * 60) + 2) % 24);
    }


    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }
}
