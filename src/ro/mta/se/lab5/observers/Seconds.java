package ro.mta.se.lab5.observers;

public class Seconds implements TimeObserver {
    private long seconds;

    public void update(long time) {
        seconds = (int) (time / 1000) % 60;
    }

    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }
}
