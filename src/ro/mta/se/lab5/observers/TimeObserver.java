package ro.mta.se.lab5.observers;


public interface TimeObserver {
    public void update(long time);
}
