package ro.mta.se.lab5.observers;


import java.util.ArrayList;
import java.util.List;

public class Timer {
    public static long counter;

    private static List<TimeObserver> observerList = new ArrayList<>();

    private static Timer ourInstance = new Timer();
    Thread myThread = new Thread(new Runnable() {
        @Override

        public void run() {
            while (!Thread.interrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Could not sleep thread " + e.getMessage());
                }
                notifyObservers();
                counter += 1000;
            }
        }
    });

    private Timer() {
    }

    public static Timer getInstance(List<TimeObserver> obsList) {

        counter = System.currentTimeMillis();
        for (TimeObserver observer : obsList) {
            attachObserver(observer);

        }
        return ourInstance;
    }

    private static void attachObserver(TimeObserver observer) {
        if (observerList.contains(observer)) {
            return;
        }

        observerList.add(observer);
    }

    public void start() {
        myThread.start();
    }

    private void notifyObservers() {
        for (TimeObserver observer : observerList) {
            if (observer != null) {
                observer.update(counter);
            }
        }
    }

    public void detachObserver(TimeObserver observer) {
        if (observerList.contains(observer)) {
            observerList.remove(observer);
        }
    }


}
