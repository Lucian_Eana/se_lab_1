package ro.mta.se.lab5.observers;

public class Minutes implements TimeObserver {
    private long minutes;

    public void update(long time) {
        minutes = (int) ((time / (1000 * 60)) % 60);
    }

    public long getMinutes() {
        return minutes;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }
}
