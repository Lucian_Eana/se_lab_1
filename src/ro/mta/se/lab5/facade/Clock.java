package ro.mta.se.lab5.facade;


import ro.mta.se.lab5.adaptor.Adapter;
import ro.mta.se.lab5.proxy.Proxy;

import java.util.Scanner;

public class Clock {
    private static volatile boolean running = true;
    Adapter adapter;
    Proxy proxy;


    public void startClock(String format) {
        proxy = new Proxy();
        if (!proxy.checkFormat(format)) {
            System.out.println("Format incorect");
            System.exit(0);
        }
        adapter = new Adapter(format);
        final Scanner keyboard = new Scanner(System.in);
        System.out.println("Press \"Enter\" to stop the clock:");
        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (running) {
                    System.out.print("\r" + adapter.getTime());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                keyboard.close();
            }
        }).start();

        String input = keyboard.nextLine();
        if (input != null && input.isEmpty()) {
            running = false;
            System.exit(0);
        }
    }
}
