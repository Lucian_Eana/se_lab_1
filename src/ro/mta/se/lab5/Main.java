package ro.mta.se.lab5;


import ro.mta.se.lab5.facade.Clock;

public class Main {

    public static void main(String[] arguments) {
        Clock clock = new Clock();
        clock.startClock(arguments[0]);
    }

}
