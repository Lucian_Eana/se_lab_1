package ro.mta.se.lab5.adaptor;


import ro.mta.se.lab5.observers.*;

import java.util.ArrayList;
import java.util.List;

public class Adapter {

    String format;
    String time;
    private List<TimeObserver> observerList;
    private TimeObserver hours;
    private TimeObserver minutes;
    private TimeObserver secundes;
    private int hour;
    private int minute;
    private int secunde;
    private Timer timer;

    public Adapter(String format) {
        observerList = new ArrayList<>();
        this.format = format;
        hours = new Hours();
        minutes = new Minutes();
        secundes = new Seconds();
        observerList.add(hours);
        observerList.add(minutes);
        observerList.add(secundes);
        timer = Timer.getInstance(observerList);
        timer.start();
    }

    public void getValue() {
        this.hour = (int) ((Hours) hours).getHours();
        this.minute = (int) ((Minutes) minutes).getMinutes();
        this.secunde = (int) ((Seconds) secundes).getSeconds();
    }

    public String getTime() {
        getValue();
        setTime(format);
        return this.time;
    }

    public void setTime(String format) {
        time = "";
        time += Integer.toString(this.hour) + ":" + Integer.toString(this.minute) + ":" + Integer.toString(this.secunde);

    }


}
