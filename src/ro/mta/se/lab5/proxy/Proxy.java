package ro.mta.se.lab5.proxy;


public class Proxy {

    public boolean checkFormat(String format) {
        String[] str = format.split(":");
        for (String item : str) {
            if (item.equals("h") || item.equals("hh") || item.equals("H") || item.equals("HH") || item.equals("m") ||
                    item.equals("mm") || item.equals("s") || item.equals("ss")) {

            } else {
                return false;
            }
        }
        return true;
    }


}
