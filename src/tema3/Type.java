package tema3;

/**
 * Created by Lucian Eana on 11/20/2016.
 */
public enum Type {
    Carnivore, Herbivore, Omnivorous;
}
