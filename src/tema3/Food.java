package tema3;

/**
 * Created by Lucian Eana on 11/20/2016.
 */
public abstract class Food {
    protected double cantitate;
    protected long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getCantitate() {
        return cantitate;
    }

    public void setCantitate(double cantitate) {
        this.cantitate = cantitate;
    }
}
