package tema3;

import tema3.FoodCarnivore;

/**
 * Created by Lucian Eana on 11/20/2016.
 */
public class Carnivore implements Animal {

    private String nume;
    private Type tip = Type.Carnivore;
    private int cotet;
    private double cantitate;
    private long interval;

    public Carnivore(String nume, int cotet, double cantitate, long interval) {
        this.nume = nume;
        this.cotet = cotet;
        this.cantitate = cantitate;
        this.interval = interval;
    }

    @Override
    public void eat() {
        long curentTime = System.currentTimeMillis() / 1000;
        long timeToEat = curentTime - this.getInterval();
        if (curentTime - timeToEat <= 3600 && this.getInterval() <= 7200)
            System.out.println(this.nume + " a mancat in ultima ora si trebuie hranit in urmatoarele 2 ore");
        if (curentTime - timeToEat > 3600 && this.getInterval() <= 7200)
            System.out.println(this.nume + " trebuie hranit in urmatoarele 2 ore");
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Type getTip() {
        return tip;
    }

    public void setTip(Type tip) {
        this.tip = tip;
    }

    public int getCotet() {
        return cotet;
    }

    public void setCotet(int cotet) {
        this.cotet = cotet;
    }

    public double getCantitate() {
        return cantitate;
    }

    public void setCantitate(double cantitate) {
        this.cantitate = cantitate;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }
}
